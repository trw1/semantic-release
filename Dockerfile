FROM node:18

RUN npm install -g
        @commitlint/cli@^17
        @commitlint/config-conventional@^17
        @semantic-release/gitlab@^9
        @semantic-release/exec@^6
        @semantic-release/git@^10
        conventional-changelog-conventionalcommits@^4
        semantic-release@^20
        semantic-release-replace-plugin@^1

